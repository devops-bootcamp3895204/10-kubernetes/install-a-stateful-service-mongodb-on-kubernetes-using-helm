# Install a stateful service MongoDB on Kubernetes using Helm

❏ Created K8s cluster on Linode Kubernetes Engine
❏ Deployed replicated MongoDB (StatefulSet using Helm Chart) and
configured Data Persistence with Linode Block Storage
❏ Deployed MongoExpress (Deployment and Service)
❏ Deployed NGINX Ingress Controller as Loadbalancer (using Helm Chart)
❏ Configured Ingress rule

